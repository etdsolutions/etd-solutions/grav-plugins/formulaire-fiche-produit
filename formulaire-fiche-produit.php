<?php
namespace Grav\Plugin;

use Grav\Common\Plugin;

/**
 * Class FormulaireFicheProduitPlugin
 * @package Grav\Plugin
 */
class FormulaireFicheProduitPlugin extends Plugin
{
    protected $gateway;

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'onPluginsInitialized' => ['onPluginsInitialized', 0]
        ];
    }

    /**
     * Enable search only if url matches to the configuration.
     */
    public function onPluginsInitialized()
    {

        if (!$this->isAdmin()) {
            $this->enable([
                'onTwigSiteVariables'          => ['onTwigSiteVariables', 0],
                'onShoppingCartPay'            => ['onShoppingCartPay', 0],
            ]);
        }
    }

    /**
     *
     */
    protected function requireGateway()
    {
        $path = realpath(__DIR__ . '/../shoppingcart/classes/gateway.php');
        if (!file_exists($path)) {
            $path = realpath(__DIR__ . '/../grav-plugin-shoppingcart/classes/gateway.php');
        }
        require_once($path);
    }

    /**
     *
     */
    public function getGateway()
    {
        if (!$this->gateway) {
            $this->requireGateway();
            require_once __DIR__ . '/gateway.php';
            $this->gateway = new ShoppingCart\GatewayCustom();
        }

        return $this->gateway;
    }

    /**
     * @param $event
     */
    public function onShoppingCartPay($event)
    {
        $this->getGateway()->onShoppingCartPay($event);
    }

    /**
     */
      public function onTwigSiteVariables()
    {
        $this->grav['assets']->addInlineJs("(function() {
        jQuery(function() {
                jQuery(document).on('proceedToPayment', function(event, ShoppingCart) {
                    setTimeout(function () {
                    $('#btn-devis').html( window.PLUGIN_SHOPPINGCART.translations.CHECKOUT_SEND);
                    $('#btn-devis').attr('disabled', null);
                }, 20);
                jQuery.ajax({
                    url: ShoppingCart.settings.baseURL + ShoppingCart.settings.urls.save_order_url + '/task:pay',
                    data: {
                        products: storejs.get('grav-shoppingcart-basket-data'),
                        data: storejs.get('grav-shoppingcart-checkout-form-data'),
                        shipping: '',
                        payment: 'custom',
                        token: '',
                        extra: {},
                        taxes: '',
                        amount: 0,
                        gateway: null
                    },
                    type: 'POST'
                })
                .success(function(redirectUrl) {
                    ShoppingCart.clearCart();
                    window.location = redirectUrl;
                })
                .error(function() {
                    alert('Erreur pendant la création de votre demande de devis.');
                });
            });
        });
    })();
");
    }
}
