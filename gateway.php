<?php
namespace Grav\Plugin\ShoppingCart;

use RocketTheme\Toolbox\Event\Event;

class GatewayCustom extends Gateway
{

    protected $name = 'custom';

    /**
     * Handle paying via this gateway
     *
     * @param Event $event
     *
     * @event onShoppingCartSaveOrder signal save the order
     * @event onShoppingCartRedirectToOrderPageUrl signal redirect to the order page
     *
     * @return mixed|void
     */
    public function onShoppingCartPay(Event $event)
    {
        $order = $this->getOrderFromEvent($event);

        $this->grav->fireEvent('onShoppingCartSaveOrder', new Event(['gateway' => $this->name, 'order' => $order]));
        $this->grav->fireEvent('onShoppingCartReturnOrderPageUrlForAjax', new Event(['gateway' => $this->name, 'order' => $order]));
    }
}


